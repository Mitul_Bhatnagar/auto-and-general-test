package org.autoandgeneral.test.api;

@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

public class NotFoundException extends ApiException {
	
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private int code;
	
    public NotFoundException(int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
