package org.autoandgeneral.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import org.springframework.validation.annotation.Validated;

/**
* <h1>Todo Not Found Error Details</h1>
* The Error details POJO for Todo Task not found. 
*
* @author  Mitul Bhatnagar
* @version 1.0
* @since   2018-05-06 
*/

@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

public class ToDoItemNotFoundErrorDetails   {
  @JsonProperty("message")
  private String message = null;

  public ToDoItemNotFoundErrorDetails message(String message) {
    this.message = message;
    return this;
  }

  /**
   * Getter of message.
   * @return message
  **/
  @ApiModelProperty(value = "")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ToDoItemNotFoundErrorDetails toDoItemNotFoundErrorDetails = (ToDoItemNotFoundErrorDetails) o;
    return Objects.equals(this.message, toDoItemNotFoundErrorDetails.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(message);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ToDoItemNotFoundErrorDetails {\n");
    
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

