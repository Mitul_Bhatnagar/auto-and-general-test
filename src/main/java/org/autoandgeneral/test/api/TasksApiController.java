package org.autoandgeneral.test.api;

import io.swagger.annotations.ApiParam;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.autoandgeneral.test.model.BalanceTestResult;
import org.autoandgeneral.test.model.ToDoItemValidationError;
import org.autoandgeneral.test.model.ToDoItemValidationErrorDetails;
import org.autoandgeneral.test.services.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TasksApiController implements TasksApi {

	private static final Logger log = LoggerFactory.getLogger(TasksApiController.class);

	private TaskService taskService;

	private final HttpServletRequest request;

	@Autowired
	public TasksApiController(TaskService taskService, HttpServletRequest request) {
		this.request = request;
		this.taskService = taskService;
	}

	/**
	 * Api Response Message.
	 * 
	 * @param input
	 *            String Input
	 * @return Response Entity
	 **/
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity tasksValidateBracketsGet(
			@NotNull @Size(min = 1, max = 50) 
			@ApiParam(value = "Input string (max length 50)", 
			required = true) @RequestParam(value = "input", 
			required = true) String input) {
		String accept = request.getHeader("Accept");
		if (accept != null && accept.contains("application/json")) {
			log.debug("Task validation Get called with value " + input);
			try {
				if (input == null || input.isEmpty() || input.length() > 50) {
					return new ResponseEntity(generateValidationError(), 
							HttpStatus.valueOf(400));
				}
				BalanceTestResult balanceTestResult = new BalanceTestResult();
				balanceTestResult.setInput(input);
				balanceTestResult.setIsBalanced(taskService
						.isParanthesesBalanced(input));
				return new ResponseEntity<BalanceTestResult>(balanceTestResult, 
						HttpStatus.OK);
			} catch (Exception e) {
				log.error("Couldn't serialize response for content " 
						+ "type application/json", e);
				return new ResponseEntity<BalanceTestResult>(
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		return new ResponseEntity<BalanceTestResult>(HttpStatus.NOT_IMPLEMENTED);
	}

	private ToDoItemValidationError generateValidationError() {
		ToDoItemValidationError toDoItemValidationError = new ToDoItemValidationError();
		toDoItemValidationError.setName("ValidationError");
		ToDoItemValidationErrorDetails toDoItemValidationErrorDetails = 
				new ToDoItemValidationErrorDetails();
		toDoItemValidationErrorDetails.setLocation("params");
		toDoItemValidationErrorDetails.setParam("text");
		toDoItemValidationErrorDetails.setMsg("Must be between 1 and 50 chars long");
		toDoItemValidationErrorDetails.setValue("");
		ArrayList<ToDoItemValidationErrorDetails> toDoItemValidationErrorDetailsList = 
				new ArrayList<>();
		toDoItemValidationErrorDetailsList.add(toDoItemValidationErrorDetails);
		toDoItemValidationError.setDetails(toDoItemValidationErrorDetailsList);
		return toDoItemValidationError;
	}

}
