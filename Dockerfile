ARG BASE_VERSION=jre-alpine
FROM openjdk:${BASE_VERSION}
MAINTAINER bhatnagarm
USER root
LABEL org.autoandgeneral.test.version="1.0.0-beta"; \
      org.autoandgeneral.test.is-beta="Yes"; \
      org.autoandgeneral.test.is-production="No"; \
      org.autoandgeneral.test.release-date="2018-05-07"
RUN mkdir -p /opt
COPY --chown=root:root target/auto-and-general-test.jar /opt/auto-and-general-test.jar
EXPOSE 8080/TCP
ENV JAVA_OPTS -Xms256m -Xmx512m -Djava.net.preferIPv4Stack=true
ENTRYPOINT java -jar -Djava.net.preferIPv4Stack=true /opt/auto-and-general-test.jar

