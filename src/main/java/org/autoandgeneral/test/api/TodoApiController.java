package org.autoandgeneral.test.api;

import io.swagger.annotations.ApiParam;

import java.math.BigDecimal;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

import org.autoandgeneral.test.model.ToDoItem;
import org.autoandgeneral.test.model.ToDoItemAddRequest;
import org.autoandgeneral.test.model.ToDoItemNotFoundError;
import org.autoandgeneral.test.model.ToDoItemNotFoundErrorDetails;
import org.autoandgeneral.test.model.ToDoItemUpdateRequest;
import org.autoandgeneral.test.model.ToDoItemValidationError;
import org.autoandgeneral.test.model.ToDoItemValidationErrorDetails;
import org.autoandgeneral.test.services.TodoRepository;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

@RestController
public class TodoApiController implements TodoApi {

    private static final Logger log = LoggerFactory.getLogger(TodoApiController.class);

    private final HttpServletRequest request;
    
    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    public TodoApiController(HttpServletRequest request) {
    	this.request = request;
    }

    /**
     * To Do get from ID.
     * 
     * @return message Response Entity
    **/
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public ResponseEntity todoIdGet(@ApiParam(value = "id",required = true) 
		@PathVariable("id") BigDecimal id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	log.debug("Todo Get task called with input value " + id.toString());
            try {
            	
            	if (todoRepository.existsById(id))
            		return new ResponseEntity<ToDoItem>(todoRepository.findById(id).get(),
            				HttpStatus.OK);
            	else 
            		return new ResponseEntity(generateNotFoundException("NotFoundError",
            				id.toString()),
            				HttpStatus.NOT_FOUND);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<ToDoItem>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<ToDoItem>(HttpStatus.NOT_IMPLEMENTED);
    }

    /**
     * Api Response Message.
     * @param id input idinput
     * @param body
     * 
    **/
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<ToDoItem> todoIdPatch(@ApiParam(value = "id",required = true) 
		@PathVariable("id") BigDecimal id,
		@ApiParam(value = "", required = true)  @RequestBody ToDoItemUpdateRequest body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	log.debug("Todo Patch task called with value " + body.toString());
            try {
            	if (body.getText() == null 
            			|| body.getText().isEmpty() || body.getText().length() > 50) {
            		return new ResponseEntity(generateValidationError(),
            				HttpStatus.valueOf(400));
            	}
            	if (todoRepository.existsById(id)) {
            		ToDoItem todoitem = todoRepository.findById(id).get();
                	todoitem.setText(body.getText());
                	todoitem.setIsCompleted(body.isIsCompleted());
                	todoitem.setCreatedAt(new DateTime(DateTimeZone.UTC).toString());
                	todoRepository.save(todoitem);
                	return new ResponseEntity<ToDoItem>(todoitem, HttpStatus.OK);
            	} else {
            		return new ResponseEntity(generateNotFoundException("NotFoundError",
            				id.toString()),HttpStatus.NOT_FOUND);
				}
            	
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<ToDoItem>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<ToDoItem>(HttpStatus.NOT_IMPLEMENTED);
    }

    /**
     * Api Response Message.
     * @param body input body
     * @return Response Entity
    **/
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity<ToDoItem> todoPost(@ApiParam(value = "", required = true) 
		@RequestBody ToDoItemAddRequest body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	log.debug("Todo Get task called with input value " + body.toString());
            try {
            	if (body.getText() == null 
            			|| body.getText().isEmpty() 
            			|| body.getText().length() > 50) {
            		return new ResponseEntity(generateValidationError(),
            				HttpStatus.valueOf(400));
            	}
            	ToDoItem todoitem = new ToDoItem();
            	todoitem.setText(body.getText());
            	todoitem.setIsCompleted(false);
            	todoRepository.save(todoitem);
            	return new ResponseEntity<ToDoItem>(todoitem, HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<ToDoItem>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<ToDoItem>(HttpStatus.NOT_IMPLEMENTED);
    }
    
    
    //TODO: Can be improved and use of Java 8 
    private ToDoItemNotFoundError generateNotFoundException(String name, String id) {
    	ToDoItemNotFoundError toDoItemNotFoundError = new ToDoItemNotFoundError();
    	toDoItemNotFoundError.setName(name);
    	ToDoItemNotFoundErrorDetails toDoItemNotFoundErrorDetail = 
    			new ToDoItemNotFoundErrorDetails();
    	toDoItemNotFoundErrorDetail.setMessage("Item with " + id + " not found");
    	ArrayList<ToDoItemNotFoundErrorDetails> toDoItemNotFoundErrorDetailsList = 
    			new ArrayList<>();
    	toDoItemNotFoundErrorDetailsList.add(toDoItemNotFoundErrorDetail);
    	toDoItemNotFoundError.setDetails(toDoItemNotFoundErrorDetailsList);
    	return toDoItemNotFoundError;
    }
    
    private ToDoItemValidationError generateValidationError() {
    	ToDoItemValidationError toDoItemValidationError = new ToDoItemValidationError();
    	toDoItemValidationError.setName("ValidationError");
    	ToDoItemValidationErrorDetails toDoItemValidationErrorDetails = 
    			new ToDoItemValidationErrorDetails();
    	toDoItemValidationErrorDetails.setLocation("params");
    	toDoItemValidationErrorDetails.setParam("text");
    	toDoItemValidationErrorDetails.setMsg("Must be between 1 and 50 chars long");
    	toDoItemValidationErrorDetails.setValue("");
    	ArrayList<ToDoItemValidationErrorDetails> toDoItemValidationErrorDetailsList = 
    			new ArrayList<>();
    	toDoItemValidationErrorDetailsList.add(toDoItemValidationErrorDetails);
    	toDoItemValidationError.setDetails(toDoItemValidationErrorDetailsList);
    	return toDoItemValidationError;
    }

}
