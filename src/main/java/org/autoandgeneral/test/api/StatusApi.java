package org.autoandgeneral.test.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.autoandgeneral.test.model.StatusResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

@Api(value = "status", description = "the status API")
public interface StatusApi {

    @ApiOperation(value = "Return system's status", nickname = "statusGet", notes = "",
    		response = StatusResponse.class, tags = { "misc" })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = StatusResponse.class) })
    @RequestMapping(value = "/status",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<StatusResponse> statusGet();

}
