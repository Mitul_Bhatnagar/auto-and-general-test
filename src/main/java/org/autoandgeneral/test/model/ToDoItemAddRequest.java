package org.autoandgeneral.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

/**
* <h1>Todo Requst Item</h1>
* Request mapping for Todo item. 
*
* @author  Mitul Bhatnagar
* @version 1.0
* @since   2018-05-06 
*/

@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

public class ToDoItemAddRequest   {
  @JsonProperty("text")
  private String text = null;

  public ToDoItemAddRequest text(String text) {
    this.text = text;
    return this;
  }

  /**
   * Getter of text.
   * @return text
  **/
  @ApiModelProperty(example = "Uulwi ifis halahs gag erh'ongg w'ssh.", value = "")

  @Size(min = 1,max = 50) 
  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ToDoItemAddRequest toDoItemAddRequest = (ToDoItemAddRequest) o;
    return Objects.equals(this.text, toDoItemAddRequest.text);
  }

  @Override
  public int hashCode() {
    return Objects.hash(text);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ToDoItemAddRequest {\n");
    
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

