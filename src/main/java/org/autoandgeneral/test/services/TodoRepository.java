package org.autoandgeneral.test.services;

import java.math.BigDecimal;

import org.autoandgeneral.test.model.ToDoItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoRepository extends CrudRepository<ToDoItem, BigDecimal> {

}
