package org.autoandgeneral.test.api;

@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

public class ApiException extends Exception {
    
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private int code;
	
    public ApiException(int code, String msg) {
        super(msg);
        this.code = code;
    }
}
