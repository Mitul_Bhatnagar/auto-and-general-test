package org.autoandgeneral.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.validation.annotation.Validated;

/**
* <h1>Todo Item Entity</h1>
* Todo Entity class used for database persistance. 
*
* @author  Mitul Bhatnagar
* @version 1.0
* @since   2018-05-06 
*/

@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")
@Entity
@Table(name = "TODOITEM")
public class ToDoItem   {
  @JsonProperty("id")
  @Id
  @SequenceGenerator(name = "todo_sequence", sequenceName = "todo_sequence", allocationSize = 1)
  @GeneratedValue(generator = "todo_sequence")
  private BigDecimal id;

  @JsonProperty("text")
  private String text;

  @JsonProperty("isCompleted")
  private Boolean isCompleted;

  @JsonProperty("createdAt")
  private String createdAt;

  public ToDoItem id(BigDecimal id) {
    this.id = id;
    return this;
  }

  @PrePersist
  @PreUpdate
  public void createdAtBeforeChange() {
	  this.createdAt = new DateTime(DateTimeZone.UTC).toString();
  }
  /**
   * Getter of id.
   * @return id
  **/

  @ApiModelProperty(example = "42.0", value = "")
  @Valid
  public BigDecimal getId() {
    return id;
  }

  public void setId(BigDecimal id) {
    this.id = id;
  }

  public ToDoItem text(String text) {
    this.text = text;
    return this;
  }

  /**
   * Getter of text.
   * @return text
  **/
  @ApiModelProperty(example = "Uulwi ifis halahs gag erh'ongg w'ssh.", value = "")

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public ToDoItem isCompleted(Boolean isCompleted) {
    this.isCompleted = isCompleted;
    return this;
  }

  /**
   * Getter of isCompleted.
   * @return isCompleted
  **/
  @ApiModelProperty(example = "false", value = "")
  public Boolean isIsCompleted() {
    return isCompleted;
  }

  public void setIsCompleted(Boolean isCompleted) {
    this.isCompleted = isCompleted;
  }

  public ToDoItem createdAt(String createdAt) {
    this.createdAt = createdAt;
    return this;
  }

  /**
   * Getter of createdAt.
   * @return createdAt
  **/
  @ApiModelProperty(example = "2017-10-13T01:50:58.735Z", value = "")
  public String getCreatedAt() {
    return createdAt;
  }

  /**
   * Setter of createdAt.
   * @param createdAt
  **/
  public void setCreatedAt(String createdAt) {
	  if (createdAt != null)
		  this.createdAt = createdAt;
	  else
		  createdAt = new DateTime(DateTimeZone.UTC).toString();
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ToDoItem toDoItem = (ToDoItem) o;
    return Objects.equals(this.id, toDoItem.id) 
    		&& Objects.equals(this.text, toDoItem.text) 
    		&& Objects.equals(this.isCompleted, toDoItem.isCompleted) 
    		&& Objects.equals(this.createdAt, toDoItem.createdAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, text, isCompleted, createdAt);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ToDoItem {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    isCompleted: ").append(toIndentedString(isCompleted)).append("\n");
    sb.append("    createdAt: ").append(toIndentedString(createdAt)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

