package org.autoandgeneral.test.configuration;

import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;

@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

@Configuration
public class SwaggerDocumentationConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Auto &amp; General test API")
            .description("This is an example implementation of API for testing candidates  - "
            		+ "You DON'T need to implement `/integrationTest` "
            		+ "and `/status` endpoints - You can run automated "
            		+ "integration tests against your API using "
            		+ "`/integrationTest` endpoint")
            .license("")
            .licenseUrl("http://unlicense.org")
            .termsOfServiceUrl("")
            .version("1.0")
            .contact(new Contact("","", ""))
            .build();
    }

    

}
