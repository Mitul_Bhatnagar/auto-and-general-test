# Auto And General Test

Implementation of Coding challenge for Auto And General 


## Overview  
This API has 2 main operations to perform 
1. An algorithm to verify whether the lexical string is balanced or not.This service takes a string in URL and returns a JSON representation back, giving the string back and result saying if it is valid or not.
2. A bunch of restful services to manage TODO tasks. The operations performed are :
	a. GET
	b. POST
	c. PATCH
These are stateful operations hence they need a persistence database to store the state. To keep the implementation simple and not have any database dependencies ,  I have used H2 in-memory database. It can be replaced easily by any commercially available databases if required.  

## To Run
This code can be run in various ways
1. Create a docker image
    Pre requisites
        Docker deamon installed
        Docker command installed
        Docker deamon has access to Internet
    Steps for Creation
        
        docker build .
        docker run -p8080:8080 <ImageName>
        
   
2. Run the build on Jenkins Server
    Pre Requisites
        Jenkins BlueOcean installed (or container running)
        Jenkins latest and patched
        Jenkins pipeline plugin installed
    Steps to execute
        
        Fork the current repository on Jenkins project and provide the git repository.
        Jenkins will clone the repository and build the required artifacts.
        Jenkins file can be updated for further upload the .jar file to Maven repository
        
        
3. Run using Maven command.
    Pre Requisites
        Maven version 3.3 and above
        jdk 8
    Steps to execute
    
        Go to command prompt.
        Run mvn clean (to clean all the pre compiled files)
        Run mvn spring-boot:run (This will start the embedded tomcat server and deploy the API on it.)
        

## Using the API
Api can be used by the user using simple curl command or to do some complicated things Postman. 
URL used to call 
Task API
    GET https://localhost:8080/test/1.0/tasks/validateBrackets?input={Input_Expression} -H  "accept: application/json"
    
Todo API
    GET https://localhost:8080/test/1.0//todo/{id} -H  "accept: application/json"
    PATCH https://localhost:8080/test/1.0//todo/{id} -H  "accept: application/json"
    POST https://localhost:8080/test/1.0/todo -H  "accept: application/json"
    
    
#Swagger Documentation 

    http://join.autogeneral.com.au/swagger-ui/?url=/swagger.json