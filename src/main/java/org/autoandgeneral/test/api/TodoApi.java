package org.autoandgeneral.test.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.math.BigDecimal;

import org.autoandgeneral.test.model.ToDoItem;

import org.autoandgeneral.test.model.ToDoItemAddRequest;
import org.autoandgeneral.test.model.ToDoItemUpdateRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

@Api(value = "todo", description = "the todo API")
public interface TodoApi {

    @ApiOperation(value = "Retrieve a specific item by id",
    		nickname = "todoIdGet",
    		notes = "", response = ToDoItem.class, tags = { "todo" })
    @RequestMapping(value = "/todo/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<ToDoItem> todoIdGet(@ApiParam(value = "id",required = true) 
    	@PathVariable("id") BigDecimal id);


    @ApiOperation(value = "Modify an item",
    		nickname = "todoIdPatch",
    		notes = "", response = ToDoItem.class, tags = { "todo" })
    @RequestMapping(value = "/todo/{id}",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.PATCH)
    ResponseEntity<ToDoItem> todoIdPatch(@ApiParam(value = "id",required = true) 
    	@PathVariable("id") BigDecimal id,
    	@ApiParam(value = "", required = true)  @RequestBody ToDoItemUpdateRequest body);


    @ApiOperation(value = "Create a to do item",
    		nickname = "todoPost", notes = "",
    		response = ToDoItem.class, tags = { "todo" })
    @RequestMapping(value = "/todo",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<ToDoItem> todoPost(@ApiParam(value = "", required = true)
    	@RequestBody ToDoItemAddRequest body);

}
