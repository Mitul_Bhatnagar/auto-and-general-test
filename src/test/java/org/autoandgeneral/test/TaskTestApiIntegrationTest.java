package org.autoandgeneral.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;

import org.autoandgeneral.test.model.BalanceTestResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * {@code @SpringBootTest} with a random port for {@link TestApi}.
 *
 * @author Mitul Bhatnagar
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class TaskTestApiIntegrationTest {

	private final ArrayList<String> brackets 
		= new ArrayList<String>(
				Arrays.asList(new String[] {"()()","[[[[[]]]]]","([)]","]{}["})); 

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testTaskPairofBraces() {
		ResponseEntity<BalanceTestResult> result 
		= this.restTemplate.getForEntity("/tasks/validateBrackets" 
		+ "?input=" + brackets.get(0), BalanceTestResult.class);
		assertTrue(result.getStatusCodeValue() == 200);
		assertTrue(result.hasBody());
		assertTrue(result.getBody().isIsBalanced());
	}

	@Test
	public void testTaskBracesInsideBraces() {
		ResponseEntity<BalanceTestResult> result 
			= this.restTemplate.getForEntity("/tasks/validateBrackets" 
					+ "?input=" + brackets.get(1), BalanceTestResult.class);
		assertTrue(result.getStatusCodeValue() == 200);
		assertTrue(result.hasBody());
		assertTrue(result.getBody().isIsBalanced());
	}

	@Test
	public void testTaskUnbalancedBraces() {
		ResponseEntity<BalanceTestResult> result 
			= this.restTemplate.getForEntity("/tasks/validateBrackets" 
					+ "?input=" + brackets.get(2), BalanceTestResult.class);
		assertTrue(result.getStatusCodeValue() == 200);
		assertTrue(result.hasBody());
		assertFalse(result.getBody().isIsBalanced());
	}

	@Test
	public void testTaskUnbalancedBracesBetweenBraces() {
		ResponseEntity<BalanceTestResult> result 
			= this.restTemplate.getForEntity("/tasks/validateBrackets" 
					+ "?input=" + brackets.get(3), BalanceTestResult.class);
		assertTrue(result.getStatusCodeValue() == 200);
		assertTrue(result.hasBody());
		assertFalse(result.getBody().isIsBalanced());
	}
}
