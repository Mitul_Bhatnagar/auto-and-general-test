CREATE TABLE TODOITEM (
	id DECIMAL GENERATED BY DEFAULT AS IDENTITY,
	text varchar(255) ,
	is_completed BOOLEAN,
	created_at varchar(25)
);

create sequence todo_sequence start with 1.0 increment by 1.0;