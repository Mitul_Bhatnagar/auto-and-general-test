package org.autoandgeneral.test.configuration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
* <h1>Home Controller</h1>
* Home Controller is the root page management. 
* This controller redirects to the swagger interface.
*
* @author  Mitul Bhatnagar
* @version 1.0
* @since   2018-05-06 
*/
@Controller
public class HomeController {
	@RequestMapping(value = "/")
    public String index() {
        return "redirect:http://join.autogeneral.com.au/swagger-ui/?url=/swagger.json";
    }
}
