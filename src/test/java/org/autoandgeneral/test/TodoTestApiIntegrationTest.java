package org.autoandgeneral.test;

import static org.junit.Assert.assertTrue;

import org.autoandgeneral.test.model.ToDoItem;
import org.autoandgeneral.test.model.ToDoItemAddRequest;
import org.autoandgeneral.test.model.ToDoItemNotFoundError;
import org.autoandgeneral.test.model.ToDoItemUpdateRequest;
import org.autoandgeneral.test.model.ToDoItemValidationError;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * {@code @SpringBootTest} with a random port for {@link TestApi}.
 *
 * @author Mitul Bhatnagar
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase
public class TodoTestApiIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;
	
	private ToDoItemAddRequest toDoItemAddRequest;
	private ToDoItemUpdateRequest todoItemUpdateRequest;

    /**
     * Initialize request objects and populate them.
     * 
    **/
	@Before
	public void before() {
		toDoItemAddRequest = new ToDoItemAddRequest();
		toDoItemAddRequest.setText("Uulwi ifis halahs gag erh'ongg w'ssh.");
		
		todoItemUpdateRequest = new ToDoItemUpdateRequest();
		todoItemUpdateRequest.setText("Uulwi ifis halahs gag erh'ongg w'ssh.");
		todoItemUpdateRequest.setIsCompleted(true);
		
	}
	
	@Test
	public void testTodoGetNotFoundException() {
		ResponseEntity<ToDoItemNotFoundError> result 
			= this.restTemplate.getForEntity("/todo/{id}", 
					ToDoItemNotFoundError.class,"42.0");
		assertTrue(result.getStatusCodeValue() == 404);
		assertTrue(result.hasBody());
		assertTrue(result.getBody().getName().equalsIgnoreCase("NotFoundError"));
	}

	@Test
	public void testTodoPostAddValue() {
		ResponseEntity<ToDoItem> result 
			= this.restTemplate.postForEntity("/todo", 
					toDoItemAddRequest, ToDoItem.class);
		assertTrue(result.getStatusCodeValue() == 200);
	}

	@Test
	public void testTodoPostInvalidValue() {
		toDoItemAddRequest.setText("123456789012345678901234567890123456789012345678901");
		ResponseEntity<ToDoItemValidationError> result 
			= this.restTemplate.postForEntity("/todo", 
					toDoItemAddRequest, ToDoItemValidationError.class);
		assertTrue(result.getStatusCodeValue() == 400);
	}
	
}
