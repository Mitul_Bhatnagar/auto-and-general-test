package org.autoandgeneral.test.services;

import java.util.Deque;
import java.util.LinkedList;

import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

	@Override
	public boolean isParanthesesBalanced(String exp) {
    	Deque<Character> deque = new LinkedList<>();
    	for (char element: exp.toCharArray()) {
    		if (element == '(' || element == '{' || element == '[')
    			deque.addFirst(Character.valueOf(element));
    		else if (element == ')' || element == '}' || element == ']') {
    			if (deque.isEmpty() || !arePair(deque.peekFirst(), element))
    				return false;
    			else 
    				deque.removeFirst();
    		}
    	}
    	
    	return deque.isEmpty() ? true : false;
	}

	/**
	 * This method verifies if the set of 
	 * parentheses are paired. 
	 */
    private boolean arePair(char opening, char closing) {
    	if (opening == '(' && closing == ')') return true;
    	else if (opening == '{' && closing == '}') return true;
    	else if (opening == '[' && closing == ']') return true;
    	return false;
    }

}
