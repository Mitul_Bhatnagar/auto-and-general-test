package org.autoandgeneral.test.api;

import org.autoandgeneral.test.model.StatusResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@javax.annotation.Generated(value = "io.swagger.codegen.languages."
		+ "SpringCodegen", date = "2018-05-01T13:25:28.119Z")

@RestController
public class StatusApiController implements StatusApi {

	private static final Logger log = LoggerFactory.getLogger(StatusApiController.class);

	/**
	 * Status Get.
	 * 
	 * @return Response Entity.
	 **/
	public ResponseEntity<StatusResponse> statusGet() {
		try {
			StatusResponse statusResponse = new StatusResponse();
			statusResponse.setStatus("healthy");
			return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Couldn't serialize response for content " 
					+ "type application/json", e);
			return new ResponseEntity<StatusResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
